package com.devsuperior.dsclients.exceptions;

public class ResourceNotFountException extends RuntimeException {
    public ResourceNotFountException(String message) {
        super(message);
    }
}
