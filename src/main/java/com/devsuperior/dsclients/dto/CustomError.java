package com.devsuperior.dsclients.dto;

import java.time.Instant;

public class CustomError {
    private Instant timestamp;
    private String message;
    private Integer status;
    private String path;

    public CustomError(Instant timestamp, String message, Integer status, String path) {
        this.timestamp = timestamp;
        this.message = message;
        this.status = status;
        this.path = path;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public Integer getStatus() {
        return status;
    }

    public String getPath() {
        return path;
    }
}
