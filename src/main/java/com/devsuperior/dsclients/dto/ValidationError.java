package com.devsuperior.dsclients.dto;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ValidationError extends CustomError {
    List<FieldMessage> errors = new ArrayList<>();

    public ValidationError(Instant timestamp, String message, Integer status, String path) {
        super(timestamp, message, status, path);
    }

    public List<FieldMessage> getErrors() {
        return errors;
    }

    public void addError(String fieldName, String message) {
        errors.add(new FieldMessage(fieldName, message));
    }
}
