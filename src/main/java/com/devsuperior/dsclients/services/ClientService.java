package com.devsuperior.dsclients.services;

import com.devsuperior.dsclients.dto.ClientDTO;
import com.devsuperior.dsclients.entities.Client;
import com.devsuperior.dsclients.exceptions.ResourceNotFountException;
import com.devsuperior.dsclients.repositories.ClientRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService {

    private ClientRepository repository;

    public ClientService(ClientRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public Page<ClientDTO> findAll(Pageable pageable) {
        Page<Client> result = repository.findAll(pageable);
        return result.map(x -> ClientDTO.entityToDto(x));
    }

    @Transactional(readOnly = true)
    public ClientDTO findById(Long id) {
        Client entity = repository.findById(id).orElseThrow(
                () -> new ResourceNotFountException("Resource not found")
        );
        return ClientDTO.entityToDto(entity);
    }

    @Transactional()
    public ClientDTO insert(ClientDTO dto) {
        Client entity = ClientDTO.dtoToEntity(dto);
        entity = repository.save(entity);
        return ClientDTO.entityToDto(entity);
    }

    @Transactional
    public ClientDTO update(Long id, ClientDTO dto) {
        try {
            Client entity = repository.getReferenceById(id);
            entity.setName(dto.getName());
            entity.setCpf(dto.getCpf());
            entity.setIncome(dto.getIncome());
            entity.setBirthDate(dto.getBirthDate());
            entity.setChildren(dto.getChildren());
            entity = repository.save(entity);
            return ClientDTO.entityToDto(entity);
        }
        catch (EntityNotFoundException e) {
            throw new ResourceNotFountException("Resource not found");
        }
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new ResourceNotFountException("Resource not found");
        }
        repository.deleteById(id);
    }
}
